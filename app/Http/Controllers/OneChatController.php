<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;

class OneChatController extends Controller
{
    public function getInfo()
    {
        $data = [
            'headers' => [
                'Authorization' => 'Bearer A83c82ffd632854faa8d6df99fa883a851f798d62d307436eb773ac2eab3701b65462c40a3951460aa6707f63a1191d4d',
                'Content-Type' => 'application/json',
            ],
            'json' => [
                'bot_id' => 'Bf2fc6164af8754d4b833c9324783b0a4',
            ],
        ];

        try {
            $client = new Client([
                'base_uri' => 'https://chat-manage.one.th:8997/api/v1/',
                'verify' => false,
            ]);

            $res = $client->request('POST', 'getlistroom', $data)->getBody()->getContents();
        } catch (ConnectException $e) {
            return $e->getMessage();
        }
        return response()->json(json_decode($res));
    }

    public function sendMsg($msg)
    {
        $data = [
            'headers' => [
                'Authorization' => 'Bearer A83c82ffd632854faa8d6df99fa883a851f798d62d307436eb773ac2eab3701b65462c40a3951460aa6707f63a1191d4d',
                'Content-Type' => 'application/json',
            ],
            'json' => [
                'to' => 'U8a08f7004b7c5625a16e4cadd3a753cf',
                'bot_id' => 'Bf2fc6164af8754d4b833c9324783b0a4',
                'type' => 'text',
                'message' => $msg,
            ],
        ];

        try {
            $client = new Client([
                'base_uri' => 'https://chat-public.one.th:8034/api/v1/',
                'verify' => false,
            ]);

            $res = $client->request('POST', 'push_message', $data)->getBody()->getContents();
        } catch (ConnectException $e) {
            return $e->getMessage();
        }
        return response()->json(json_decode($res));
    }
}
