<?php

namespace App\Http\Controllers\User;

use App\Facebook;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
//        // แบบที่ 1
//        $user = \App\User::where('email', $request->email)
//            ->get()->first();
//
//        if (Hash::check(
//            $request->password, $user->password
//        )) {
//            // แบบที่ 1.1
////            Auth::login($user);
//
//            // แบบที่ 1.2
//            Auth::loginUsingId($user->id);
//
//            return redirect()->route('home');
//        }

        // แบบที่ 2
        $isAuth = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ]);

        if (!$isAuth) {
            return redirect()->back();
        }
        return redirect()->route('home');

// scope //
//        $user = \App\User::where([
//            'email' => $request->email,
//        ])->get(['password', 'name']);

    }

    public function logout()
    {
        Auth::logout();
        return redirect()
            ->route('book.index');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallBack()
    {
        $facebookUser = Socialite::driver('facebook')
            ->stateless()->user();

        $userExist = User::where('email', $facebookUser->email)->get()->first();

        if (empty($userExist)) {

            $user = new User();
            $user->name = $facebookUser->name;
            $user->email = $facebookUser->email;
            $user->org_auth = 'facebook';
            $user->org_id = $facebookUser->id;
            $user->save();

            $facebook = new Facebook();
            $facebook->id = $facebookUser->id;
            $facebook->token = $facebookUser->token;
            $facebook->refresh_token = $facebookUser->refreshToken;
            $facebook->expires_in = $facebookUser->expiresIn;
            $facebook->avatar_original = $facebookUser->avatar_original;
            $facebook->created_by = $user->id;
            $facebook->save();

            Auth::login($user);

        } else {
           Auth::login($userExist);
        }
        return redirect()->route('home');
    }
}
