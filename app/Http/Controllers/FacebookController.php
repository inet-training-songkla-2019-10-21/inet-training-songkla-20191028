<?php

namespace App\Http\Controllers;

use App\Facebook;
use App\FacebookConnection\FacebookSDK;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FacebookController extends Controller
{
    public function info()
    {
//        $user = Auth::user();
//        $accessToken = Facebook::where('id', $user->org_id)
//            ->get(['token'])
//            ->first();
//        dd($accessToken->token);
//        dd(Auth::user()->with('facebook')->get());
        $accessToken = Auth::user()->facebook->token;
        $res = FacebookSDK::sdkConnect(
            $accessToken,
            'me/posts?fields=full_picture,message,shares'
            );
        return response()->json(json_decode($res->getBody()));
    }
}
