<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::all();
        return view('book.index', compact('books'));
    }

    public function createPage()
    {
        return view('book.create');
    }

    public function create(Request $request)
    {
        $book = new Book();
        $book->name = $request->name;
        $book->author = $request->author;
        $book->price = $request->price;
        $book->describe = $request->describe;
        $book->type = $request->type;

        if (!$book->save()) {
            return redirect()->route('book.create.page');
        }
        return redirect()->route('book.index');
    }

    public function editPage($id)
    {
        $book = Book::find($id);
        return view('book.edit', ['book' => $book]);
    }

    public function edit(Request $request)
    {
        $book = Book::find($request->id);
        $book->name = $request->name;
        $book->author = $request->author;
        $book->price = $request->price;
        $book->describe = $request->describe;
        $book->type = $request->type;

        if (!$book->save()) {
            return redirect()->route('book.edit.page');
        }
        return redirect()->route('book.index');
    }

    public function delete(Book $id)
    {
//        if (empty(Auth::user())){
//            return redirect()->route('login');
//        }
        $id->delete();
        return redirect()->route('book.index');
    }
}
