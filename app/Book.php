<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'name',
        'author',
        'price',
        'describe',
        'type',
    ];
//    protected $guarded = [];

    public function user()
    {
        return $this->belongsToMany(User::class, 'created_by', 'id');
    }
}
