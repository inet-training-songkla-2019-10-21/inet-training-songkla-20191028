<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Book Store</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}">--}}
{{--    <script src="{{asset('js/app.js')}}"></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <style>
        body {
            background: #032c41;
            color: #ffffff;
            margin-top: 40px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }

    </style>
</head>
<body>
<br><br>
<div class="container">
    <div class="row">
        <div class="col-3">
            <h1><a href="{{route('book.index')}}" class="main"><strong>Book Store</strong></a></h1>
        </div>
    </div>
    <br><br>
    <div class="row justify-content-center">
        <div class="col-6" align="left" style="margin: 30px">
            <h2 style="color: #f3f3f3;"><u>แบบฟอร์มแก้ไขหนังสือ</u></h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form action="{{route("book.edit")}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="bookname">หนังสือ - นิตยสาร</label>
                    <input
                        type="text" class="form-control"
                        name="name" id="bookname"
                        aria-describedby="bookHelp"
                        placeholder="ชื่อ หนังสือ - นิตยสาร"
                        value="{{$book->name}}"
                        required
                    >
                    <small id="bookHelp" class="form-text text-muted">ชื่อหนังสือที่คุณต้องการบันทึก</small>
                </div>
                <div class="form-group">
                    <label for="booktype">ประเภทหนังสือ</label>
                    <select multiple class="form-control" id="booktype" name="type">
                        <option>สารคดี</option>
                        <option>บันเทิง</option>
                        <option>develop</option>
                        <option>นิตยสาร</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="author">ผู้เขียน</label>
                    <input type="text" class="form-control" name="author" id="author" value="{{$book->author}}" placeholder="ชื่อ ผู้เขียน">
                </div>
                <div class="form-group">
                    <label for="describe">คำบรรยาย</label>
                    <textarea type="text" class="form-control" id="describe" name="describe">{{$book->describe}}</textarea>
                </div>

                <div class="form-group">
                    <label for="author">ราคา</label>
                    <input type="number" class="form-control" id="price" value="{{$book->price}}"  name="price" placeholder="ราคา" required>
                </div>
                <div class="form-group">
                    <input type="hidden" name="id" value="{{$book->id}}">
                </div>
                <br>
                <div align="right">
                    <button type="submit" class="btn btn-success">บันทึกการเปลี่ยนแปลง</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
