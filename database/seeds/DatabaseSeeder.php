<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Book::class, 10)->create();
        factory(\App\User::class)->create([
            'email' => 'aaaa@aaaa.com',
            'password' => \Illuminate\Support\Facades\Hash::make('password')
        ]);
    }
}
