<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookController@index')->name('book.index');

Route::group(['middleware' => ['user']], function () {
    Route::get('book/delete/{id}','BookController@delete')->name('book.delete');
    Route::get('/book/create', 'BookController@createPage')->name('book.create.page');
    Route::post('/book/create', 'BookController@create')->name('book.create');
    Route::get('/book/edit/{id}','BookController@editPage')->name('book.edit.page');
    Route::post('/book/edit','BookController@edit')->name('book.edit');

    Route::get('/facebook/profile', 'FacebookController@info');

});

Route::get('/onechat', 'OneChatController@getInfo');
Route::get('/onechat/message/{msg}', 'OneChatController@sendMsg');

Route::get('/login/facebook',
    'User\LoginController@redirectToProvider'
)->name('login.facebook');

Route::get('/login/facebook/callback',
    'User\LoginController@handleProviderCallBack'
);




// Authentication Routes...
Route::get('login', 'User\LoginController@showLoginForm')->name('login');
Route::post('login', 'User\LoginController@login');


Route::post('logout', 'User\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'User\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'User\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Email Verification Routes...
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify'); // v6.x
/* Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify'); // v5.x */
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');




Route::get('/home', 'HomeController@index')->name('home');
