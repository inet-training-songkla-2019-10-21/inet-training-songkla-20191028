<?php

namespace Tests\Feature;

use App\Book;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGuestCanSeeBookPage()
    {
        $response = $this->get(route('book.index'));

        $response->assertViewIs('book.index')
            ->assertStatus(200);
    }

    public function testGuestCanSeeAllBookExist()
    {
        $books = factory(Book::class, 5)->create();

        $response = $this->get(route('book.index'))
            ->assertSuccessful();

        $response->assertSee($books[0]->name);
        $response->assertSee($books[1]->name);
        $response->assertSee($books[2]->name);
        $response->assertSee($books[3]->name);
        $response->assertSee($books[4]->name);

    }

    public function testUserCanAddNewBook()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $book = factory(Book::class)->make();

        $this->get(route('book.create.page'))
            ->assertViewIs('book.create')
            ->assertStatus(200);

        $this->post(route('book.create'), [
            '_token' => \Session::token(),
            'name' => $book->name,
            'author' => $book->author,
            'price' => $book->price,
            'describe' => $book->describe,
            'type' => $book->type,
        ])->assertRedirect(route('book.index'));

        $this->assertDatabaseHas('books', [
            'name' => $book->name,
            'author' => $book->author,
            'price' => $book->price,
            'describe' => $book->describe,
            'type' => $book->type,
        ]);
    }

    public function testUserCanEditBook()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $book = factory(Book::class)->create();
        $newBook = factory(Book::class)->make();

        $this->get(route('book.edit.page', $book->id))
            ->assertViewIs('book.edit')
            ->assertSee($book->name)
            ->assertSee($book->price)
            ->assertStatus(200);

        $this->post(route('book.edit'), [
            '_token' => \Session::token(),
            'id' => $book->id,
            'name' => $newBook->name,
            'author' => $newBook->author,
            'price' => $newBook->price,
            'describe' => $newBook->describe,
            'type' => $newBook->type,
        ])->assertRedirect(route('book.index'));

        $bookEdited = Book::find($book->id);
        $this->assertEquals($newBook->name, $bookEdited->name);
        $this->assertEquals($newBook->author, $bookEdited->author);
        $this->assertEquals($newBook->price, $bookEdited->price);
        $this->assertEquals($newBook->describe, $bookEdited->describe);
        $this->assertEquals($newBook->type, $bookEdited->type);
    }

    public function testUserCanDeleteBook()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $book = factory(Book::class)->create();

        $this->get(route('book.delete', $book->id))
            ->assertRedirect(route('book.index'));

        $bookDeleted = Book::find($book->id);
        $this->assertEmpty($bookDeleted);
    }
}
