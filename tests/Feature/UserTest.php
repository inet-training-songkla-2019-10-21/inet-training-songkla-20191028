<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testGuestCanSeeLoginPage()
    {
        $this->get(route('login'))
            ->assertViewIs('auth.login')
            ->assertStatus(200);
    }

    public function testUserCanLogin()
    {
        $user = factory(User::class)->create();

        $this->get(route('login'))
            ->assertSuccessful();

        $this->post(route('login'), [
            '_token' => \Session::token(),
            'email' => $user->email,
            'password' => 'password',
        ])->assertRedirect(route('home'));

        $this->assertNotEmpty(Auth::user());
    }

    public function testUserCanLogout()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $this->get(route('home'))->assertSuccessful();

        $this->post(route('logout'), [
            '_token' => \Session::token(),
        ])->assertRedirect(route('book.index'));

        $this->assertEmpty(Auth::user());
    }

    public function testGuestCanSeeRegisterPage()
    {
        $this->get(route('register'))
            ->assertViewIs('auth.register')
            ->assertStatus(200);
    }

    public function testGuestCanRegister()
    {
        $user = factory(User::class)->make();

        $this->get(route('register'))
            ->assertSuccessful();

        $this->post(route('register'), [
            '_token' => \Session::token(),
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
        ])->assertRedirect(route('login'));

        $isUser = User::where('email', $user->email)->exists();
        $this->assertTrue($isUser);
    }
    
}
