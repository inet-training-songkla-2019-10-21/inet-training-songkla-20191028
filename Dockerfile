FROM docker.entercorp.net/entercorp/php-nginx:7.3

WORKDIR /var/www/html

COPY . ./

COPY nginx.conf /etc/nginx/conf.d/default.conf

RUN composer install

